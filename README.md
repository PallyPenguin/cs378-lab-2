# CS378 Lab 2

## Group 8:
* Evan Peng
* Noah Kuhn 
* Christian Macneill

## Branching Structure
### Master
* Merge location for feature branches
### Freeze
* Release branch for grading, merged from Master
### Feature Branches
* Features will be split up into their own branches to avoid merge conflicts
    * E.g. "ball_physics", "car_design", etc.
* Merged together in Master 
